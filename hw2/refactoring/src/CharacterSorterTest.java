import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class CharacterSorterTest {
	private Data character = new Data();
	private Sorter sorter = new Sorter();
	private ArrayList<Integer> dataList = new ArrayList<Integer>();
	private ArrayList<Integer> mySortedList;
	private int numberOfData = 10;

	@Before
	public void setUpBefore() throws Exception {
		List<java.lang.Character> list = Arrays.asList('A', 'K', 'S', 'o', 'p', 'R', 'I', 'c', 'x', 'e');
		character.setNumberOfData(numberOfData);
		sorter.setData(character);
		for (int i = 0; i < numberOfData; i++) {
			int value = (int) list.get(i);
			dataList.add(value);
		}
		character.setDataList(dataList);
	}

	@Test
	public void testCharacterInIncreasing() {
		Collections.sort(dataList);
		mySortedList = sorter.sortDataWith(Sorter.IncreasingComparator);
		for (int i = 0; i < numberOfData; i++) {
			assertTrue(dataList.get(i) == mySortedList.get(i));
		}
	}

	@Test
	public void testCharacterInDecreasing() {
		Collections.sort(dataList, Collections.reverseOrder());
		mySortedList = sorter.sortDataWith(Sorter.DecreasingComparator);
		for (int i = 0; i < numberOfData; i++) {
			assertTrue(dataList.get(i) == mySortedList.get(i));
		}
	}
}
