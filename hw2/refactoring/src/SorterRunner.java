import java.util.*;

public class SorterRunner {
	public static void main(String[] args) {
		Menu menu = new Menu();
		int selectedMenu;
		do {
			selectedMenu = menu.selectMenu();
			menu.runSelectedMenu(selectedMenu);
		} while (selectedMenu != Menu.QUIT);
	}
}

class Menu {
	public static final int INPUT_DATA = 1;
	public static final int INCREASING_ORDER = 2;
	public static final int DECREASING_ORDER = 3;
	public static final int QUIT = 4;
	public static final int NUMBER = 1;
	public static final int CHARACTER = 2;
	private Scanner scanner = new Scanner(System.in);
	private int selectedDataType;
	private Data data;
	private Sorter sorter;

	public int selectMenu() {
		displayMenu();
		int selectedMenu = scanner.nextInt();
		return selectedMenu;
	}

	public void displayMenu() {
		System.out.println("[ ID: 1412213 ]");
		System.out.println("[ Name: Seo Min Ji ]");
		System.out.println("1. Input data");
		System.out.println("2. Print data in increasing order");
		System.out.println("3. Print data in decreasing order");
		System.out.println("4. Quit\n");
		System.out.print("> ");
	}

	public void runSelectedMenu(int selectedMenu) {
		if (selectedMenu == INPUT_DATA) {
			inputData();
		} else if (selectedMenu == INCREASING_ORDER) {
			sorter.sortDataWith(Sorter.IncreasingComparator);
			printSortedDataList();
		} else if (selectedMenu == DECREASING_ORDER) {
			sorter.sortDataWith(Sorter.DecreasingComparator);
			printSortedDataList();
		} else if (selectedMenu == QUIT) {
			quit();
		}
	}

	public void inputData() {
		selectDataType();
		initializeDataAndSorter();
		inputNumberOfData();
		makeDataList();
	}

	public void selectDataType() {
		System.out.print("> The type of data (n or c): ");
		char type = scanner.next().charAt(0);
		if (type == 'c') {
			this.selectedDataType = CHARACTER;
		} else if (type == 'n') {
			this.selectedDataType = NUMBER;
		}
	}

	public void initializeDataAndSorter() {
		data = new Data();
		sorter = new Sorter();
		sorter.setData(data);
	}

	public void inputNumberOfData() {
		System.out.print("> The number of data: ");
		int numberOfData = scanner.nextInt();
		data.setNumberOfData(numberOfData);
	}

	public void makeDataList() {
		ArrayList<Integer> dataList = new ArrayList<Integer>();
		System.out.print("> data: ");
		if (selectedDataType == NUMBER) {
			dataList = makeNumberList();
		} else if (selectedDataType == CHARACTER) {
			dataList = makeCharacterList();
		}
		System.out.println();
		data.setDataList(dataList);
	}

	public ArrayList<Integer> makeNumberList() {
		int numberOfData = data.getNumberOfData();
		ArrayList<Integer> dataList = new ArrayList<Integer>();
		for (int i = 0; i < numberOfData; i++) {
			dataList.add(scanner.nextInt());
		}
		return dataList;
	}

	public ArrayList<Integer> makeCharacterList() {
		int numberOfData = data.getNumberOfData();
		ArrayList<Integer> dataList = new ArrayList<Integer>();
		for (int i = 0; i < numberOfData; i++) {
			int data = (int) scanner.next().charAt(0);
			dataList.add(data);
		}
		return dataList;
	}

	public void printSortedDataList() {
		if (this.selectedDataType == NUMBER) {
			printSortedNumberList();
		} else if (this.selectedDataType == CHARACTER) {
			printSortedCharacterList();
		}
	}

	public void printSortedNumberList() {
		ArrayList<Integer> sortedNumberList = data.getDataList();
		for (int i = 0; i < data.getNumberOfData(); i++) {
			System.out.print(sortedNumberList.get(i) + " ");
		}
		System.out.println("\n");
	}

	public void printSortedCharacterList() {
		ArrayList<Integer> sortedCharacterList = data.getDataList();
		for (int i = 0; i < data.getNumberOfData(); i++) {
			char value = castToCharacter(sortedCharacterList.get(i));
			System.out.print(value + " ");
		}
		System.out.println("\n");
	}

	private char castToCharacter(int value) {
		return (char) value;
	}

	public void quit() {
		System.out.println("!! QUIT !!");
	}
}

class Data {
	private ArrayList<Integer> dataList = new ArrayList<Integer>();
	private int numberOfData;

	public void setNumberOfData(int numberOfData) {
		this.numberOfData = numberOfData;
	}

	public void setDataList(ArrayList<Integer> dataList) {
		this.dataList = dataList;
	}

	public int getNumberOfData() {
		return numberOfData;
	}

	public ArrayList<Integer> getDataList() {
		return dataList;
	}
}

interface Comparator {
	boolean compare(int current, int next);
}

class Sorter {
	public static final Comparator IncreasingComparator = (current, next) -> current > next;
	public static final Comparator DecreasingComparator = (current, next) -> current < next;
	private Data data;
	private int numberOfData;
	private ArrayList<Integer> dataList;

	public void setData(Data data) {
		this.data = data;
	}

	public ArrayList<Integer> sortDataWith(Comparator comparator) {
		setDataInfomation(data);
		for (int i = 0; i < numberOfData - 1; i++) {
			for (int j = 0; j < numberOfData - 1 - i; j++) {
				if (comparator.compare(dataList.get(j), dataList.get(j + 1))) {
					Collections.swap(dataList, j, j + 1);
				}
			}
		}
		return dataList;
	}

	public void setDataInfomation(Data data) {
		this.numberOfData = data.getNumberOfData();
		this.dataList = data.getDataList();
	}
}