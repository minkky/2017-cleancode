import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class NumberSorterTest {
	private Data number = new Data();
	private Sorter sorter = new Sorter();
	private ArrayList<Integer> dataList, mySortedList;
	private int numberOfData = 10;

	@Before
	public void setUpBefore() throws Exception {
		this.dataList = new ArrayList<Integer>(Arrays.asList(1, 10, 5, 3, 2, 4, 7, 6, 8, 9));
		number.setNumberOfData(numberOfData);
		sorter.setData(number);
		number.setDataList(dataList);
	}

	@Test
	public void testNumberInIncreasing() {
		Collections.sort(dataList);
		mySortedList = sorter.sortDataWith(Sorter.IncreasingComparator);
		for (int i = 0; i < numberOfData; i++) {
			assertTrue(dataList.get(i) == mySortedList.get(i));
		}
	}

	@Test
	public void testNumberInDecreasing() {
		Collections.sort(dataList, Collections.reverseOrder());
		mySortedList = sorter.sortDataWith(Sorter.DecreasingComparator);
		for (int i = 0; i < numberOfData; i++) {
			assertTrue(dataList.get(i) == mySortedList.get(i));
		}
	}
}