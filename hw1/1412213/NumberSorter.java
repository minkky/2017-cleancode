import java.util.InputMismatchException;
import java.util.Scanner;

interface Comparator {
	boolean compare(int current, int next);
}

public class NumberSorter {
	private static final int INPUT_NUMBERS = 1;
	private static final int PRINT_NUMBERS_IN_INCREASING_ORDER = 2;
	private static final int PRINT_NUMBERS_IN_DECREASING_ORDER = 3;
	private static final int QUIT = 4;
	private final Comparator IncreasingComparator = (current, next) -> current > next;
	private final Comparator DecreasingComparator = (current, next) -> current < next;
	private Scanner scanner = new Scanner(System.in);
	private int numberOfNumbers = -1;
	private int[] numbers;

	public static void main(String[] args) {
		NumberSorter numberSorter = new NumberSorter();
		int selectedMenu;
		try {
			while (true) {
				selectedMenu = numberSorter.selectMenu();
				if (selectedMenu == QUIT)
					break;
				numberSorter.runSelectedMenu(selectedMenu);
			}
		} catch(InputMismatchException inputMismatchException) {
			System.out.println("Exception : " + inputMismatchException);
		}
	}
	
	public int selectMenu() throws InputMismatchException {
		displayMenu();
		int selectedMenu = scanner.nextInt();
		return selectedMenu;
	}
	
	public void displayMenu() {
		System.out.println("[ ID: 1412213 ]");
		System.out.println("[ Name: Seo Min Ji ]");
		System.out.println("1. Input numbers");
		System.out.println("2. Print numbers in increasing order");
		System.out.println("3. Print numbers in decreasing order");
		System.out.println("4. Quit\n");
		System.out.print("> ");
	}

	public void runSelectedMenu(int selectedMenu) throws InputMismatchException {
		if (selectedMenu == INPUT_NUMBERS) {
			inputNumbers();
		} else if (selectedMenu == PRINT_NUMBERS_IN_INCREASING_ORDER) {
			sortNumbersWith(IncreasingComparator);
			printSortedNumbers();
		} else if (selectedMenu == PRINT_NUMBERS_IN_DECREASING_ORDER) {
			sortNumbersWith(DecreasingComparator);
			printSortedNumbers();
		}
	}

	public void inputNumbers() {
		System.out.print("> The number of numbers: ");
		numberOfNumbers = scanner.nextInt();
		numbers = new int[numberOfNumbers];
		System.out.print("> numbers: ");
		for (int i = 0; i < numberOfNumbers; i++) {
			numbers[i] = scanner.nextInt();
		}
		System.out.print("\n");
	}
	
	public void sortNumbersWith(Comparator comparator) {
		for (int i = 0; i < numberOfNumbers - 1; i++) {
			for (int j = 0; j < numberOfNumbers - 1 - i; j++) {
				if (comparator.compare(numbers[j], numbers[j + 1])) {
					swapCurrentAndNext(j, j+1);
				}
			}
		}
	}
	
	public void swapCurrentAndNext(int current, int next) {
		int temp = numbers[current];
		numbers[current] = numbers[next];
		numbers[next] = temp;
	}

	public void printSortedNumbers() {
		for (int i = 0; i < numberOfNumbers; i++) {
			System.out.print(numbers[i] + " ");
		}
		System.out.println("\n");
	}
}